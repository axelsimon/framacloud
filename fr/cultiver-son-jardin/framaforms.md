# Installation de Framaforms

Le tutoriel d'installation est en cours de rédaction.

Pour les plus pressés, commencez par installer un [Drupal 7](https://drupal.org) +
[Webform](https://www.drupal.org/project/webform) +
[Form Builder](https://www.drupal.org/project/form_builder)
et à jouer avec sur votre hébergement web (vous pouvez utilisez
[Drush](http://docs.drush.org/en/master/install/), qui vous fera gagner
un temps précieux).

Toutes nos excuses pour le délai nous faisons vraiment de notre mieux !

L'équipe Framasoft.